import os
from typing import Optional, Text, List
from absl import logging
from ml_metadata.proto import metadata_store_pb2
from tfx.orchestration import metadata
from tfx.orchestration import pipeline
from tfx.orchestration.local.local_dag_runner import LocalDagRunner

from tfx.types import Channel

from tfx.proto import pusher_pb2
from tfx.proto import trainer_pb2
from tfx.proto import example_gen_pb2

from tfx.dsl.components.base import executor_spec

from tfx.components.trainer import executor as trainer_executor
from tfx.components import ExampleValidator
from tfx.components import SchemaGen
from tfx.components import StatisticsGen
from tfx.components import Trainer
from tfx.components import ImportExampleGen

# from tfx.components import Pusher
# from tfx.components import ResolverNode
from tfx.components import Transform

# from tfx.components import Evaluator

from . import image_transform

dirname = os.path.dirname(__file__)

PIPELINE_NAME = "obj_det_pipeline"
PIPELINE_ROOT = os.path.join(".", "pipeline_output")
METADATA_PATH = os.path.join(".", "tfx_metadata", PIPELINE_NAME, "obj-det-metadata.db")
ENABLE_CACHE = True


def create_pipeline(
    pipeline_name: Text,
    pipeline_root: Text,
    # enable_cache: bool,
    # run_fn: Text,
    training_file: Text,
    transform_file: Text,
    data_path: Text,
    train_args: trainer_pb2.TrainArgs,
    eval_args: trainer_pb2.EvalArgs,
    metadata_connection_config: Optional[metadata_store_pb2.ConnectionConfig] = None,
    beam_pipeline_args: Optional[List[Text]] = None,
):
    components = []

    output = example_gen_pb2.Output(
        split_config=example_gen_pb2.SplitConfig(
            splits=[
                example_gen_pb2.SplitConfig.Split(name="train", hash_buckets=3),
                example_gen_pb2.SplitConfig.Split(name="eval", hash_buckets=1),
            ]
        )
    )

    example_gen = ImportExampleGen(input_base=data_path, output_config=output)
    components.append(example_gen)

    # Computes statistics over data for visualization and example validation.
    statistics_gen = StatisticsGen(
        examples=example_gen.outputs["examples"],
        exclude_splits=["eval"],
    )
    components.append(statistics_gen)

    # Generates schema based on statistics files.
    schema_gen = SchemaGen(
        statistics=statistics_gen.outputs["statistics"],
        exclude_splits=["eval"],
        infer_feature_shape=True,
    )
    components.append(schema_gen)

    # Performs anomaly detection based on statistics and data schema.
    example_validator = ExampleValidator(  # pylint: disable=unused-variable
        statistics=statistics_gen.outputs["statistics"],
        schema=schema_gen.outputs["schema"],
        exclude_splits=["eval"],
    )
    components.append(example_validator)

    transform = Transform(
        examples=example_gen.outputs["examples"],
        schema=schema_gen.outputs["schema"],
        module_file=transform_file,
    )
    components.append(transform)

    # Uses user-provided Python function that
    # implements a model using Tensorflow.
    trainer = Trainer(
        module_file=training_file,
        # examples=example_gen.outputs["examples"],
        # Use outputs of Transform as training inputs if Transform is used.
        examples=transform.outputs["transformed_examples"],
        transform_graph=transform.outputs["transform_graph"],
        schema=schema_gen.outputs["schema"],
        train_args=train_args,
        eval_args=eval_args,
        custom_executor_spec=executor_spec.ExecutorClassSpec(
            trainer_executor.GenericExecutor
        ),
    )
    components.append(trainer)

    return pipeline.Pipeline(
        pipeline_name=pipeline_name,
        pipeline_root=pipeline_root,
        components=components,
        # enable_cache=enable_cache,
        metadata_connection_config=metadata_connection_config,
        beam_pipeline_args=beam_pipeline_args,
    )


def run_pipeline():
    pipeline = create_pipeline(
        pipeline_name=PIPELINE_NAME,
        pipeline_root=PIPELINE_ROOT,
        enable_cache=ENABLE_CACHE,
        metadata_connection_config=metadata.sqlite_metadata_connection_config(
            METADATA_PATH
        ),
    )

    LocalDagRunner().run(pipeline)


if __name__ == "__main__":
    logging.set_verbosity(logging.INFO)
    run_pipeline()
